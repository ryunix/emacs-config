;;; init.el  -*- lexical-binding: t; -*-

;;; Code:

(defvar elpaca-installer-version 0.10)
(defvar elpaca-directory (expand-file-name "elpaca/" user-emacs-directory))
(defvar elpaca-builds-directory (expand-file-name "builds/" elpaca-directory))
(defvar elpaca-repos-directory (expand-file-name "repos/" elpaca-directory))
(defvar elpaca-order '(elpaca :repo "https://github.com/progfolio/elpaca.git"
                              :ref nil :depth 1 :inherit ignore
                              :files (:defaults "elpaca-test.el" (:exclude "extensions"))
                              :build (:not elpaca--activate-package)))
(let* ((repo  (expand-file-name "elpaca/" elpaca-repos-directory))
       (build (expand-file-name "elpaca/" elpaca-builds-directory))
       (order (cdr elpaca-order))
       (default-directory repo))
  (add-to-list 'load-path (if (file-exists-p build) build repo))
  (unless (file-exists-p repo)
    (make-directory repo t)
    (when (<= emacs-major-version 28) (require 'subr-x))
    (condition-case-unless-debug err
        (if-let* ((buffer (pop-to-buffer-same-window "*elpaca-bootstrap*"))
                  ((zerop (apply #'call-process `("git" nil ,buffer t "clone"
                                                  ,@(when-let* ((depth (plist-get order :depth)))
                                                      (list (format "--depth=%d" depth) "--no-single-branch"))
                                                  ,(plist-get order :repo) ,repo))))
                  ((zerop (call-process "git" nil buffer t "checkout"
                                        (or (plist-get order :ref) "--"))))
                  (emacs (concat invocation-directory invocation-name))
                  ((zerop (call-process emacs nil buffer nil "-Q" "-L" "." "--batch"
                                        "--eval" "(byte-recompile-directory \".\" 0 'force)")))
                  ((require 'elpaca))
                  ((elpaca-generate-autoloads "elpaca" repo)))
            (progn (message "%s" (buffer-string)) (kill-buffer buffer))
          (error "%s" (with-current-buffer buffer (buffer-string))))
      ((error) (warn "%s" err) (delete-directory repo 'recursive))))
  (unless (require 'elpaca-autoloads nil t)
    (require 'elpaca)
    (elpaca-generate-autoloads "elpaca" repo)
    (load "./elpaca-autoloads")))
(add-hook 'after-init-hook #'elpaca-process-queues)
(elpaca `(,@elpaca-order))

(elpaca elpaca-use-package
  (elpaca-use-package-mode))

(use-package css-mode
  :defer t
  :custom
  (css-indent-offset 2))

(use-package ebuild-mode
  :ensure (:repo "https://gitweb.gentoo.org/proj/ebuild-mode.git")
  :defer t)

(use-package emacs
  :custom
  (indent-tabs-mode nil)
  (inhibit-startup-screen t)
  (initial-scratch-message nil))

(use-package emacs
  :if (display-graphic-p)
  :custom-face
  (default ((t (:family "Cica" :height 135))))
  (fixed-pitch ((t nil)) face-defface-spec)
  (fixed-pitch-serif ((t nil)) face-defface-spec)
  (variable-pitch ((t nil)) face-defface-spec)
  :config
  (set-fontset-font t 'unicode (font-spec :family "Cica")))

(use-package evil
  :ensure t
  :custom
  (evil-undo-system 'undo-redo)
  (evil-want-C-h-delete t)
  (evil-want-C-u-delete t)
  (evil-want-C-u-scroll t)
  (evil-want-empty-ex-last-command nil)
  (evil-want-keybinding nil)
  :config
  (evil-mode))

(use-package evil-collection
  :ensure t
  :after evil
  :config
  (evil-collection-init))

(use-package evil-numbers
  :ensure t
  :after evil
  :bind
  (:map evil-normal-state-map
        ("C-c -" . evil-numbers/dec-at-pt)
        ("C-c =" . evil-numbers/inc-at-pt))
  (:map evil-visual-state-map
        ("C-c -" . evil-numbers/dec-at-pt)
        ("C-c =" . evil-numbers/inc-at-pt)
        ("C-c C--" . evil-numbers/dec-at-pt-incremental)
        ("C-c C-=" . evil-numbers/inc-at-pt-incremental)))

(use-package kkc
  :unless (executable-find "mozc_emacs_helper")
  :defer t
  :custom
  (default-input-method "japanese"))

(use-package magit
  :ensure t
  :defer t)

(use-package markdown-mode
  :ensure t
  :defer t)

(use-package menu-bar
  :config
  (menu-bar-mode -1))

(use-package modus-themes
  :ensure t
  :if (display-graphic-p)
  :config
  (load-theme 'modus-vivendi :no-confirm))

(use-package mozc
  :ensure t
  :if (executable-find "mozc_emacs_helper")
  :defer t
  :custom
  (default-input-method "japanese-mozc"))

(use-package scroll-bar
  :if (display-graphic-p)
  :config
  (scroll-bar-mode -1))

(use-package tool-bar
  :if (display-graphic-p)
  :config
  (tool-bar-mode -1))

(use-package transient
  :ensure t
  :defer t)

(use-package treesit-auto
  :ensure t
  :custom
  (treesit-auto-install 'prompt)
  :config
  (treesit-auto-add-to-auto-mode-alist 'all)
  (global-treesit-auto-mode))

(use-package vdiff
  :ensure t
  :defer t)

;; Local Variables:
;; indent-tabs-mode: nil
;; End:

;;; init.el ends here
