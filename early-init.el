;;; early-init.el  -*- lexical-binding: t; -*-

;;; Code:

(setq package-enable-at-startup nil)

;; Local Variables:
;; indent-tabs-mode: nil
;; End:

;;; early-init.el ends here
